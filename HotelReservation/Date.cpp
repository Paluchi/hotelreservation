#include "Date.h"
#include <iostream>
#include <string>

Date::Date()
	:mDay(0),
	mMonth(0),
	mYear(0)
{}
Date::Date(int mnth, int dd, int yyyy)
{
	mMonth=mnth;
	mDay=dd;
	mYear=yyyy;

}
Date::Date(const Date &date){
	mDay=date.mDay;
	mMonth=date.mMonth;
	mYear=date.mYear;
}
int Date::getDay() const {return mDay;}
int Date::getMonth() const {return mMonth;}
int Date::getYear() const {return mYear;}
//setters
void Date::setDay(const int dd){ mDay=dd;}
void Date::setMonth(const int mm){mMonth=mm;}
void Date::setYear(const int yyyy){mYear=yyyy;}

//displays obtained date
void Date::displayDate()
{
	std::cout<< getDay() <<"-"<< getMonth() <<"-"<< getYear() <<std::endl;
}

std::string Date::getStringDate()
{
	return std::to_string(mDay) + '-' + std::to_string(mMonth) + '-' + std::to_string(mYear);
}

std::ostream& operator<<(std::ostream& write, const Date& date)
{
	write << date.mDay << "\n"
		<< date.mMonth << "\n"
		<< date.mYear;
	return write;
}

std::istream& operator>>(std::istream& read, Date& date)
{
	read >> date.mDay
		>> date.mMonth
		>> date.mYear;
	
	return read;
}

