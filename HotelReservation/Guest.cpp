#include "Guest.h"
#include <iostream>

std::string Guest::getRoomNum() const 
{ 
	return mRoomNum;
}

void Guest::setRoomNum(const std::string& roomNum) { mRoomNum = roomNum; }

//creates an Guest Spa appointment
void Guest::makeSpaReservation()
{
	Spa::makeSpaReservation();
	std::string input{};

	std::cout<<"\nFirst Name: "<< std::endl;
	std::getline(std::cin, input);
	setFName(input);
	std::cout<< "\nLast Name: "<< std::endl;
	std::getline(std::cin, input);
	setLName(input);
	std::cout << "\nService Required: "<< std::endl;
	std::getline(std::cin, input);
	setService(input);
	std::cout<<"\nRoom Number: "<< std::endl;
	std::getline(std::cin, input);
	setRoomNum(input);
}

std::ofstream& operator<<(std::ofstream& write, const Guest& guest)
{
	write << guest.mFName << std::endl
		<< guest.mLName << std::endl
		<< guest.mResTime << std::endl
		<< guest.mResDate << std::endl
		<< guest.mPhoneNumber << std::endl
		<< guest.mService << std::endl
		<< guest.mRoomNum;

	return write;
}

std::ifstream& operator>>(std::ifstream& read, Guest& guest)
{
	read >> guest.mFName
		>> guest.mLName
		>> guest.mResTime
		>> guest.mResDate
		>> guest.mPhoneNumber
		>> guest.mService
		>> guest.mRoomNum;
	return read;
}