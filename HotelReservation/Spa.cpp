#include "Spa.h"
#include <iostream>

//constructors
Spa::Spa()
{
	setResDate(Date());
	setResTime("2:00");
	setService(" ");
	setFName("");
	setLName(" ");
}
Spa::Spa(Date &date, std::string& resTme, std::string& srvce, std::string& fName, std::string&  lName)
	:mResDate(date),
	mResTime(mResTime),
	mService(srvce),
	mFName(fName),
	mLName(lName)
	
{
}

//accessers
Date Spa::getResDate() const 
{
	return mResDate;
}
std::string Spa::getResTime() const 
{
	return mResTime;
}
std::string Spa::getService() const 
{
	return mService;
}
std::string Spa::getFName() const 
{
	return mFName;
}
std::string Spa::getLName() const 
{
	return mLName;
}

CellNumber Spa::getPhoneNumber() const
{
	return mPhoneNumber;
}

//setters
void Spa::setResDate(Date &spaDate)
{ 
	mResDate = spaDate;
}
void Spa::setResTime(const std::string& resTime)
{
	mResTime = resTime;
}    
void Spa::setService(const std::string& service)
{
	mService = service;
}
void Spa::setFName(const std::string& fName)
{
	mFName=fName;
}
void Spa::setLName(const std::string& lName)
{
	mLName=lName;
}

void Spa::setPhoneNumber(const CellNumber& number)
{
	mPhoneNumber = number;
}

//Methods
//TODO ADD date and time here
void Spa::makeSpaReservation()
{
	std::string input{};

	std::cout<<"First Name: \n";
	std::getline(std::cin, input);
	setFName(input);
	std::cout<< "Last Name: \n";
	std::getline(std::cin, input);
	setLName(input);
	std::cout << "\nService Required:\n";
	std::getline(std::cin, input);
	setService(input);
	
}
/**/
std::string Spa::validateTime()
{
	//get current date / time
	// check if date is greater than current
	// if date is the same, ensure greater than current time;
	// if date is greater than current, any time is good 

	return {};
}
/*
bool Spa::checkInt(char *str){
	
	int val =0;
	bool status= false;

	while (str[val]){

		if(isalpha(str[val]))
		{
			status =true;
			cout<< "PLEASE ENTER CORRECT FORMAT"<<endl;
			validateTime();
		}

		val ++;
	}
	return status;
}
*/

/*
std::ofstream& operator<<(std::ofstream& write, const Spa& spa)
{
	write << spa.mFName
		<< spa.mLName
		<< spa.mResTime
		<< spa.mResDate
		<< spa.mPhoneNumber
		<< spa.mService;

	return write;
}

std::ifstream& operator>>(std::ifstream& read, Spa& spa)
{
	read >> spa.mFName
		>> spa.mLName
		>> spa.mResTime
		>> spa.mResDate
		>> spa.mPhoneNumber
		>> spa.mService;
	return read;
}
*/