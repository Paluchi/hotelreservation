#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ReservationManager.h"
#include "Account.h"
# define h1 "SEASONS HOTEL"
#define h2 "SERVICE OF EXCEPTIONAL QUALITY"
# define clrscrn system("cls")
#define invalid "Invalid Selection"

class MainWindow
{

public:
	MainWindow();
	~MainWindow() = default;
	void heading();

	//prints to screen the main options
	char headingOption();

	//gets reservation choice
	char hotelOptions();

	char makeRoomReservation();

	char getAllHotelRoomReservations();

	char getReservation(const std::string& key);

	char updateAReservation();
	
	//Spa treatment options Walkin /guests
	char spaOption();

	char makeGuest();
	
	char makeWalk_In();

	void finalWindow();
	
	bool login();

private:
	std::unique_ptr<ReservationManager> mResManager;
	std::unique_ptr<Account> mAccount;
};
#endif
