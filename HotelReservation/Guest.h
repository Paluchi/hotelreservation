#ifndef GUEST_H
#define GUEST_H

#include <fstream>
#include "CellNumber.h"
#include "Spa.h"

class Guest : public Spa{

	static constexpr const char* guestFile{ "Guest.txt" };
	std::string mRoomNum;

public:
	Guest() = default;
	std::string Guest::getRoomNum() const ;

	void Guest::setRoomNum(const std::string& roomNum);

	void makeSpaReservation() override;
	

	friend std::ofstream& operator<<(std::ofstream& write, const Guest& guest);

	friend std::ifstream& operator>>(std::ifstream& read, Guest& guest);
};
#endif