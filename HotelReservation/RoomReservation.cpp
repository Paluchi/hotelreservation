#include "RoomReservation.h"
#include "Validation.h"
#include <iostream>


RoomReservation::RoomReservation(std::string& fname, 
								std::string& lname,
								std::string& roomNum, 
								std::unique_ptr<Date> date, 
								int nights, int persons, 
								std::unique_ptr<CellNumber> cell)
	: mFName(fname),
	mLName(lname),
	mRoomNum(roomNum),
	mDate(std::move(date)),
	mNumNights(nights),
	mNumPerson(persons),
	mPhoneNumber(std::move(cell))
{
}

RoomReservation::RoomReservation()
	:mFName(""),
	mLName(""),
	mRoomNum(""),
	mDate(std::make_unique<Date>()),
	mNumNights(0),
	mNumPerson(0),
	mPhoneNumber(std::make_unique<CellNumber>())
{
}

RoomReservation::RoomReservation(const RoomReservation& res)
	:mFName(res.mFName),
	mLName(res.mLName),
	mRoomNum(res.mRoomNum),
	mDate(std::move(res.getDate())),
	mNumNights(res.mNumNights),
	mNumPerson(res.mNumPerson),
	mPhoneNumber(std::move(res.getPhoneNum()))
{
}


RoomReservation& RoomReservation::operator=(const RoomReservation& res)
{
	if (this != &res)
	{
		mFName = res.mFName;
		mLName = res.mLName;
		mRoomNum = res.mRoomNum;
		mDate = std::move(res.getDate());
		mNumNights = res.mNumNights;
		mNumPerson = res.mNumPerson;
		mPhoneNumber = std::move(res.getPhoneNum());
	}
	return *this;
}

std::ifstream& operator>>(std::ifstream& read, RoomReservation& res)
{
	std::unique_ptr<Date> tDate = std::make_unique<Date>();
	std::unique_ptr<CellNumber> tCellNumber = std::make_unique<CellNumber>();
	read >> res.mFName
		>> res.mLName
		>> res.mRoomNum
		>>  *tDate
		>> res.mNumNights
		>> res.mNumPerson
		>> *tCellNumber;

	res.mDate = std::move(tDate);
	res.mPhoneNumber = std::move(tCellNumber);

	return read;
}

std::ofstream& operator<<(std::ofstream& write, const RoomReservation& res)
{
	write << res.mFName << std::endl
		<< res.mLName << std::endl
		<< res.mRoomNum << std::endl
		<< res.mDate << std::endl
		<< res.mNumNights << std::endl
		<< res.mNumPerson << std::endl
		<< res.mPhoneNumber;
	return write;
}

//setters
void RoomReservation::setFname(const std::string& fname){this->mFName =fname;}
void RoomReservation::setLname(const std::string& lname){this->mLName=lname;}
void RoomReservation::setDate(std::unique_ptr<Date> date){mDate=std::move(date);}
void RoomReservation::setNumNights(int nights){this->mNumNights=nights;}
void RoomReservation::setNumPerson(int person){this->mNumPerson=person;}
void RoomReservation::setRoomNum(const std::string& rmNum){this->mRoomNum=rmNum;}
void RoomReservation::setPhoneNum(std::unique_ptr<CellNumber> cell){mPhoneNumber=std::move(cell);}

//getters
std::string RoomReservation::getFname()const {return this->mFName;}
std::string RoomReservation::getLname()const{return this->mLName;}
std::unique_ptr<Date> RoomReservation::getDate()const { 

	return std::make_unique<Date>(*mDate);
}; //TODO fix this
int RoomReservation::getNumNights()const {return this->mNumNights;}
int RoomReservation::getNumPersons()const {return this->mNumPerson;}
std::string RoomReservation::getRoomNum()const {return this->mRoomNum;}
std::unique_ptr<CellNumber> RoomReservation::getPhoneNum() const //TODO fix this
{
	return std::make_unique<CellNumber>(*mPhoneNumber);
}

RoomReservation RoomReservation::getInfo()
{
	bool status=false;
	RoomReservation room;
	Validation validator;
	static char num[]= "876-549-5693";
	std::string info{ "" };
	int val =0;

	/*room.setDate(validDate());
	cout<< room.date.getDay() <<endl;
	cout<< room.date.getMonth()<< endl; 
	cout <<room.date.getYear() <<endl;

	//room.phoneNumber.separate(num);
	//cout<< room.phoneNumber.getAreaCode() <<endl;
	//cout<< room.phoneNumber.getAreaCode() + "-" + room.phoneNumber.getXchange()+ "-" + room.phoneNumber.getLine() << endl;
	
	*/
	

	/**/
	std::cout<< "\n First Name: ";
	std::cin>> info;
	room.setFname(info);
	
	std::cout<< " \n LastName: ";
	std::cin>> info;
	room.setLname(info);
	
	
	do {
		val = 1;
		std::cout << "\nNumber of nights: ";
		std::cin >> info;
		try {
			room.setNumNights(stoi(info));
			val = 0;
		}catch (const std::invalid_argument& ) {

			std::cerr << "Please Enter a Numeric value greater than 0:\n ";
			val = 1;
		} catch (const std::out_of_range&) {

			std::cerr << "Number of nights exceeds allowed amount. \n We will give you 100 nights\n";
			val = 0;
			room.setNumNights(100);
		}

	} while (0 != val);

	do {
		val = 1;
		std::cout << "\nNumber of persons: ";
		std::cin >> info;
		try
		{
			room.setNumPerson(std::stoi(info));
			val = 0;
		}
		catch (const std::invalid_argument&) {

			std::cerr << "Please Enter a Numeric value greater than 0:\n ";
			val = 1;
		}
		catch (const std::out_of_range&) {

			std::cerr << "Number of persons exceeds allowed amount. \n Maximum allowed is 50\n";
			val = 0;
			room.setNumPerson(50);
		}
	} while (val != 0);

	do{
		val = 1;
		std::cout << "\n Cell Number:  Please use the format XXX-XXX-XXXX\n";
		std::cin >> info;
		CellNumber phone;
		bool result = validator.isPhoneFormatValid(info);
		if (!result)
		{
			std::cout << "\n ############## phone number is INVALID##################\nEXITITING\n";
			std::getline(std::cin, info);
		}
		else
		{
			room.getPhoneNum()->setAreaCode(std::string(info.begin(), info.begin() + 3));
			room.getPhoneNum()->setXchange(std::string(info.begin()+5, info.begin() + 7));
			room.getPhoneNum()->setLine(std::string(info.begin()+9, info.end()));
			val = 0;
		}
	} while (val != 0);
	
	Date date;
	static int iterCnt{ 0 };
	val = 1;

	do {
		if (3 < iterCnt)
			break;
		std::cout << "\n Reservation Date:  Please use the format DD-MM-YYYY\n";
		std::cin >> info;
		if (validator.checkDate(date, info))
		{
			val = 0;
		}
		++iterCnt;

	} while (val != 0);
		
	room.setDate(std::make_unique<Date>(date));

	std::cout<<"\nRoom Number: "<< std::endl;
	std::cin>>info;
	room.setRoomNum(info);

	//return recordReservation(fileName, room);
	return room;
}
//saves a reservation
bool RoomReservation::recordReservation(const std::string& filename, RoomReservation& room)
	{
		bool status =false;
		

		std::ofstream  write(filename, std::ios::app);

		if (!write)
		{
			std::cerr<<"\n Unable to Create Reservation" << std::endl;
		}else{
			write << room.getFname() << std::endl
				<< room.getLname() << std::endl
				<< room.getRoomNum() << std::endl
				<< room.getDate()  << std::endl
				<< room.getNumNights() << std::endl
				<< room.getNumPersons() << std::endl
				<< room.getPhoneNum();
			//cout<< "\nReservation Reserved for " << room.getFname() << " "<< room.getLname()<<endl;
			status = true;
		}

		write.close();
		return status;
	}
//searches for a specific record and returns value if found
 RoomReservation RoomReservation::getReservation(const std::string& filename, const std::string& key){

	std::string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	 RoomReservation room;
	 CellNumber cell =CellNumber();
	 Date date =Date();
	
	 std::ifstream read(filename, std::ios::in);

	if(!read)
	{
		std::cerr << "Unable to Locate Reservation" << std::endl;
	}else{
		while(!read.eof())
		{
			read >>  fname;
			read >> lname;
			read >> rmNum;
			read >> day;
			read >> mnth;
			read >>year;
			read >> nNghts;
			read >> nPrsns;
			read >> aCode;
			read >> xChnge;
			read >> line;
		
			if((aCode+xChnge+line)==key)
			{
				date.setDay(day);
				date.setMonth(mnth);
				date.setYear(year);
				cell.setAreaCode(aCode);
				cell.setXchange(xChnge); 
				cell.setLine(line);
				room.setFname(fname); 
				room.setLname(lname);
				room.setDate(std::make_unique<Date>(date));
				room.setRoomNum(rmNum);
				room.setPhoneNum(std::make_unique<CellNumber>(cell)); 
				room.setNumNights(nNghts); 
				room.setNumPerson(nPrsns);
				break;
			}
		}
	}
	read.close();	
	return room;
}
 //displays values in a reservation
void RoomReservation::display(const RoomReservation& room)  const
{
	std::cout << "\nFirst Name: " << room.getFname() <<"\nLast Name: "<< room.getLname() << "\nReservation Date: "<<room.getDate()->getDay() <<'-'<< room.getDate()->getMonth()
		<<'-'<< room.getDate()->getYear()<< "\nNumber of Nights: "<<room.getNumNights()<< "\nNumber of Persons: "<<room.getNumPersons() <<"\nRoom Number: "<< room.getRoomNum() 
		<<"\nPhone Number: " << room.getPhoneNum()->getAreaCode() <<'-'<< room.getPhoneNum()->getXchange()<<'-'<< room.getPhoneNum()->getLine() << std::endl;

}

void RoomReservation::display() const
{
	std::cout << "\nFirst Name: " << getFname()
		<< "\nLast Name: " << getLname() << "\nReservation Date: "
		<<getDate()->getDay() << '-' <<getDate()->getMonth()
		<< '-' << getDate()->getYear() << "\nNumber of Nights: "
		<< getNumNights() << "\nNumber of Persons: " <<
		getNumPersons() << "\nRoom Number: " << getRoomNum()
		<< "\nPhone Number: " <<getPhoneNum()->getAreaCode() << '-'
		<<getPhoneNum()->getXchange() << '-' << getPhoneNum()->getLine() << std::endl;


}
//displayes all hotel reservations
void RoomReservation::displayAllReservation(const std::string& fileName) const
{
	std::string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	 RoomReservation room;
	 CellNumber cell =CellNumber();
	 Date date =Date();
	
	 std::ifstream read(fileName, std::ios::in);

	if(!read)
	{
		std::cerr << "Unable to Locate Reservations" << std::endl;
	}else{
		while(true)
		{
			
			read >>  fname;
			read >> lname;
			read >> rmNum;
			read >> day;
			read >> mnth;
			read >>year;
			read >> nNghts;
			read >> nPrsns;
			read >> aCode;
			read >> xChnge;
			read >> line;
			if(read.eof())
			{
				break;
			}
			date.setDay(day);
			date.setMonth(mnth); 
			date.setYear(year);
			cell.setAreaCode(aCode);
			cell.setXchange(xChnge);
			cell.setLine(line);

			room.setFname(fname); 
			room.setLname(lname);
			room.setDate(std::make_unique<Date>(date));
			room.setRoomNum(rmNum); 
			room.setPhoneNum(std::make_unique<CellNumber>(cell));
			room.setNumNights(nNghts); 
			room.setNumPerson(nPrsns);
			
  			display(room);
		}

	}
	read.close();	
}

void RoomReservation::reservation(const std::string& filename)
{
	std::string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	 RoomReservation room;
	 CellNumber cell =CellNumber();
	 Date date =Date();
	
	 std::ifstream read(filename, std::ios::in);

	if(!read)
	{
		std::cerr << "Unable to Locate Reservation" << std::endl;

	}else{	

		read >>  fname;
		read >> lname;
		read >> rmNum;
		read >> day;
		read >> mnth;
		read >>year;
		read >> nNghts;
		read >> nPrsns;
		read >> aCode;
		read >> xChnge;
		read >> line;		
			
		date.setDay(day); 
		date.setMonth(mnth);
		date.setYear(year);
		cell.setAreaCode(aCode);
		cell.setXchange(xChnge); 
		cell.setLine(line);
		room.setFname(fname);
		room.setLname(lname);
		room.setDate(std::make_unique<Date>(date));
		room.setRoomNum(rmNum);
		room.setPhoneNum(std::make_unique<CellNumber>(cell)); 
		room.setNumNights(nNghts); 
		room.setNumPerson(nPrsns);
				
		
	}
	read.close();	

}
//updates the reservation
bool RoomReservation::updateReservation(const std::string& filename)
{
	bool status = false;
	char temp[]="           ";
	RoomReservation room;
	CellNumber cell =CellNumber();

	std::cout << "Client's Phone Number: "<< std::endl;
	std::cin>>temp;

	//temp=room.getPhoneNum().remover(temp);

	if(isdigit(temp[0]))
	{
		//temp=(char *)atoi(temp);
		
		room=getReservation(filename,temp); //searches for record and display to screen
		if((room.getFname())!= "")		
		display(room);

		if(tempFile(room,filename)) //updates located record
		{
			status=true;
			remove("reservation.txt");
			auto result = rename("temp.txt", "reservation.txt");
		}

	}else
	{
		std::cout << "Incorrect number format entered"<< std::endl;
	}


	return status;
}


bool RoomReservation::tempFile(RoomReservation& room, const std::string& filename)
{
	bool result =false;
	RoomReservation roomTemp;
	Date date=Date();
	CellNumber cell=CellNumber();
	std::string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	std::ifstream read = std::ifstream(filename, std::ios::in);
	std::string tempTxt{ "temp.txt" };

	if(!read)
	{
		std::cerr<<"Unable to locade database"<< std::endl;
	}else
	{
		while(!read.eof())
		{
			read >>  fname;
			read >> lname;
			read >> rmNum;
			read >> day;
			read >> mnth;
			read >>year;
			read >> nNghts;
			read >> nPrsns;
			read >> aCode;
			read >> xChnge;
			read >> line;
			date.setDay(day); 
			date.setMonth(mnth);
			date.setYear(year);
			cell.setAreaCode(aCode); 
			cell.setXchange(xChnge); cell.setLine(line);
			roomTemp.setFname(fname);
			roomTemp.setLname(lname); 
			roomTemp.setDate(std::make_unique<Date>(date));
			roomTemp.setRoomNum(rmNum);
			roomTemp.setPhoneNum(std::make_unique<CellNumber>(cell)); 
			roomTemp.setNumNights(nNghts); 
			roomTemp.setNumPerson(nPrsns);
			//display(roomTemp);

			if( (room.getPhoneNum()->getAreaCode()
				+room.getPhoneNum()->getXchange()
				+room.getPhoneNum()->getLine() )
				!= (aCode+xChnge+line) )
			{
				recordReservation(tempTxt, roomTemp);
			}
		}

		recordReservation(tempTxt, room);
		result=true;
		/*string filename2="reservation.txt";
		string tempFile1= "temp.txt";
		if( remove( "reservation.txt" ) != 0 )
		{
			result=false;
		}else
		{
			result =true;
		}
//		remove("reservation.txt");
		//cout<<val<<endl;
		if(val)
		{
			if(rename(tempFile1.c_str(), filename2.c_str())==0)
				result=true;
		}*/
	}
	return result;
}













/*
void RoomReservation::deleteReservation(string filename, RoomReservation room)
{
	//bool status= false;
	string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	 RoomReservation room1;
	 CellNumber cell =CellNumber();
	 Date date =Date();
	
	 ifstream read(filename, ios::in);

	if(!read)
	{
		cerr << "Unable to Locate Reservations" <<endl;
	}else{
		while(!read.eof())
		{			
			read >>  fname;
			read >> lname;
			read >> rmNum;
			read >> day;
			read >> mnth;
			read >>year;
			read >> nNghts;
			read >> nPrsns;
			read >> aCode;
			read >> xChnge;
			read >> line;

			
			if((aCode+xChnge+line)!=(room.getPhoneNum().getAreaCode()+room.getPhoneNum().getXchange()+room.getPhoneNum().getLine()))
			{
				date.setDay(day); date.setMonth(mnth); date.setYear(year);
				cell.setAreaCode(aCode); cell.setXchange(xChnge); cell.setLine(line);
				room1.setFname(fname); room1.setLname(lname); room1.setDate(date); room.setRoomNum(rmNum);
				room1.recordReservation("temp.txt", room1);
			}
		}

	}
	read.close();	

}

bool RoomReservation::updateReservation(string filename, RoomReservation room)
{
	bool status= false;

	string fname=" ", lname=" ", rmNum=" ", aCode=" ",xChnge=" ", line=" ";
	int nNghts=0, day=0,mnth=0, year=0, nPrsns=0;
	 RoomReservation room1;
	 CellNumber cell =CellNumber();
	 Date date =Date();

	 room.deleteReservation(filename, room);
	 room.recordReservation(filename,room);
	
	 ifstream read("temp.txt", ios::in);

	if(!read)
	{
		cerr << "Unable to Locate Reservations" <<endl;
	}else{
		
		while(!read.eof())
		{			
			read >>  fname;
			read >> lname;
			read >> rmNum;
			read >> day;
			read >> mnth;
			read >>year;
			read >> nNghts;
			read >> nPrsns;
			read >> aCode;
			read >> xChnge;
			read >> line;

			date.setDay(day); date.setMonth(mnth); date.setYear(year);
			cell.setAreaCode(aCode); cell.setXchange(xChnge); cell.setLine(line);
			room1.setFname(fname); room1.setLname(lname); room1.setDate(date); room.setRoomNum(rmNum);
			room1.recordReservation(filename, room1);
		}
		cout<<"record Updated"<<endl;
	}
	return status;
	
}

//validates entered date
Date RoomReservation::validDate()
{
	int day1=0;
	int day=0,  month=0, month1=8, year1=0, year =0, num=0;
	time_t now = time(0);
	tm *ltm =localtime(&now);

	char str[5]="    "; 
	day= ltm->tm_mday;
	month= 1 + ltm->tm_mon;
	year = 1900 + ltm->tm_year;

	while((day1 < day) || (month1 < month) || (year1 < year )) 
	{
		cout<< "Reservation Date:  " << "\n" << "Format  " << "dd mm yyyy"<<endl;
		cout<< "Day:  ";
		cin >>str;
		if(!checkInt(str))
			day1=stoi(str);		

		cout<< "Month:  ";
		cin >> str;
		if(!checkInt(str))
			month1= stoi(str);		
		

		cout<< "Year:  ";
		cin >> str;
		if(!checkInt(str))
			year1 = stoi(str);
	}

	Date resDate(day1, month1, year1);
	return resDate;
}

//check to ensure that value entered is numberic
bool RoomReservation::checkInt(char *str){
	
	int val =0;
	bool status= false;

	while (str[val]){

		if(isalpha(str[val]))
		{
			status =true;
			cout<< "PLEASE ENTER CORRECT FORMAT"<<endl;
			validDate();
		}

		val ++;
	}
	return status;
}

//sectionalizes number
CellNumber RoomReservation::separate(char *number){
	char *temp="";
	const int max=50;
	char  areaCode[max];
	char xchange[max];
	char line[max];
	int b=0, a=0,c=0;//i;terators
	
	temp=remover(number);

		for(int i=0; i <10; i++)
		{
			if(i<3){
				areaCode[a]=temp[i];
				a++;
			}
			if(i == 3)
				areaCode[a]='\0';
			
			if(i>2 && i<6){				
				xchange[b]=temp[i];
				b++;				
			}
			if(i == 6)
				xchange[b]='\0';

			if(i>5 && i<10){
				line[c]=temp[i];
				c++;				
			}
			if(i == 9)
				line[c]='\0';			
		}

		CellNumber cell = CellNumber();
		cell.setAreaCode(areaCode);
		cell.setXchange(xchange);
		cell.setLine(line);	
	return cell;		

}

//removes spaces and dashes
char * RoomReservation::remover(char * num){
	int cnt=0;
		
	for(int i=0; i < (signed)strlen(num); i++)
	{
		if((num[i] != ' ') && (num[i] != '-'))
			num[cnt++] = num[i];
	}
	num[cnt]='\0';

	return num;
}
*/


