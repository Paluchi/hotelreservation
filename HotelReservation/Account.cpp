#include "Account.h"
#include <fstream>
#include <iostream>

Account::Account()
	:mUsername(""),
	mPassword("")
{	
}
std::string Account::getPassword() const {return mPassword;}
std::string Account::getUsername() const {return mUsername;}
void Account::setPassword(const std::string& pass){mPassword = pass;}
void Account::setUsername(const std::string& user){mUsername = user;}
//adds a user
bool Account::createUser()
{
	bool status=false;

	std::ofstream write("users.txt", std::ios::app);
	std::string pass="", user="";
	std::cout<<"username"<<std::endl;
	std::cin>>user;
	std::cout<<"\nPassword"<<std::endl;
	std::cin>>pass;
	setPassword(pass);
	setUsername(user);

	if(write)
	{
		write<<getUsername()<<std::endl;
		write<<getPassword()<<std::endl;
		status=true;
	}else
	{
		status=false;
	}

	write.close();
	return status;

}
//validates user credentials
bool Account::validUser(const std::string& username, const std::string& password)
{
	bool status=false;

	std::string pass{}, user{};

	std::ifstream read("users.txt", std::ios::in);
	
	if(read)
	{		
		while (!read.eof())
		{
			read >> user;
			read >> pass;

			if ((username == user) &&
				(password == pass))
			{
				setUsername(user);
				setPassword(pass);
				status = true;
				break;
			}
		}

		read.close();
	}
	return status;
}

void Account::displayUser() const 
{
	std::cout<<"\n"<<getUsername()<<std::endl;
}