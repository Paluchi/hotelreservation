#ifndef WALK_IN_H
#define WALK_IN_H
#include "Spa.h"
#include <iostream>
#include <fstream>

class Walk_In : public Spa{
public:
	
	void makeSpaReservation() override;

	friend std::ofstream& operator<<(std::ofstream& write, const Walk_In& walkIn);

	friend std::ifstream& operator>>(std::ifstream& read, Walk_In& walkIn);
};
#endif