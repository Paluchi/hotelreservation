#ifndef ROOMRESRVATION_H	
#define ROOMRESRVATION_H

#include "Date.h"
#include "CellNumber.h"
#include <memory>

class RoomReservation final{

private:
	std::string mFName;
	std::string mLName;
	std::string mRoomNum;
	std::unique_ptr<Date> mDate;
	int mNumNights;
	int mNumPerson;
	std::unique_ptr<CellNumber> mPhoneNumber;

public:
	//Contructors
	RoomReservation(std::string& fname, std::string& lname, std::string& roomNum, std::unique_ptr<Date> date, int nights, int persons, std::unique_ptr<CellNumber> cell);
	RoomReservation();
	RoomReservation(const RoomReservation& res);
	~RoomReservation() = default;

	friend std::ifstream& operator>>(std::ifstream& stream, RoomReservation& res);
	friend std::ofstream& operator<<(std::ofstream& write, const RoomReservation& res);
	RoomReservation& operator=(const RoomReservation& res);

	//Accessors
	std::string getFname() const;
	std::string getLname() const;
	std::unique_ptr<Date> getDate() const;
	int getNumNights() const;
	int getNumPersons() const;
	std::string getRoomNum() const;
	std::unique_ptr<CellNumber> getPhoneNum() const;
	//Setters
	void setFname(const std::string& fname);
	void setLname(const std::string& lname);
	void setDate(std::unique_ptr<Date> date);
	void setNumNights(int);
	void setNumPerson(int);
	void setRoomNum(const std::string& rmNum);
	void setPhoneNum(std::unique_ptr<CellNumber> cell);

	bool recordReservation(const std::string&, RoomReservation&);
	RoomReservation getReservation(const std::string& ,const std::string&);
	RoomReservation getInfo();
	Date validDate();
	bool checkInt(char *);
	void clientInfo();
	void displayAllReservation(const std::string& fileName) const;
	CellNumber cellNum();
	CellNumber separate(char *);
	char *remover(char*);
	void display(const RoomReservation& room ) const ;
	void display() const ;
	void reservation(const std::string&);
	bool updateReservation(const std::string&);
	bool tempFile(RoomReservation&, const std::string&);

};
#endif
