#ifndef ACCOUNT_H
#define ACCOUNT_H
#include <string>

class Account
{
private :
	std::string mUsername;
	std::string mPassword;

public:
	Account();
	std::string getUsername() const ;
	std::string getPassword() const ;

	void setPassword(const std::string& pass);
	void setUsername(const std::string& user);

	bool createUser();

	bool validUser(const std::string& username, const std::string& password);

	void displayUser() const;

};
#endif