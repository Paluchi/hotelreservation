#ifndef SAVE_H
#define SAVE_H
#include <string>
#include <fstream>
#include <iostream>
#include "RoomReservation.h"


class Save{

public:
	void recordReservation(std::string filename, RoomReservation room)
	{
		std::ofstream  write(filename, std::ios::out);

		if (!write)
		{
			std::cerr<<"\n Unable to Create Reservation" << std::endl;
		}else{
			write << room.getFname()<<std::endl;
			write << room.getLname()<< std::endl;
			write << room.getRoomNum()<< std::endl;
			write << room.getDate()->getDay()<< std::endl;
			write << room.getDate()->getMonth()<< std::endl;
			write << room.getDate()->getYear()<< std::endl;
			write << room.getNumNights()<< std::endl;
			write << room.getNumPersons()<< std::endl;
			write << room.getNumNights()<< std::endl;
			write << room.getPhoneNum()->getAreaCode()<< std::endl;
			write << room.getPhoneNum()->getXchange() << std::endl;
			write << room.getPhoneNum()->getLine() << std::endl;
		}

		write.close();
	}

	/*
	*/

};

#endif;