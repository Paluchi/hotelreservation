#ifndef SPA_H
#define SPA_H

#include <string>
#include "Date.h"
#include "CellNumber.h"
#include "Validation.h"
class Spa
{
public:

	Spa(Date&, std::string& resTime, std::string& service, std::string& fName, std::string& lName);
	Spa();

	virtual ~Spa() = default;
	//mutators
	Date getResDate() const;
	std::string getResTime() const;
	std::string getService() const;
	std::string getFName() const;
	std::string getLName() const;
	CellNumber getPhoneNumber() const;
	//Setters
	void setResDate(Date&);
	void setResTime(const std::string& resTime);
	void setService(const std::string& service);
	void setFName(const std::string& fName);
	void setLName(const std::string& lName);
	void setPhoneNumber(const CellNumber& number);

	std::string validateTime();


	virtual void makeSpaReservation();
	
protected:
	Date mResDate;
	std::string mResTime;
	std::string mService;
	std::string mFName;
	std::string mLName;
	CellNumber mPhoneNumber;
	Validation mValidator;

};
#endif