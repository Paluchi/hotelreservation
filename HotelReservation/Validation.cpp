#include "Validation.h"
#include <sstream>



//TODO verify date LeapYear 
bool Validation::checkDate(Date& date, std::string& inDate)
{
	time_t now = time(0);
	tm* ltm = localtime(&now);

	const int day{ ltm->tm_mday };
	const int month{ 1 + ltm->tm_mon };
	const int year{ 1900 + ltm->tm_year };

	bool validDate{ false };
	
	if (isValidDateFormat(inDate))
	{
		std::stringstream stream(inDate);
		int inDay{ 0 }, inMonth{ 0 }, inYear{ 0 };
		char separator;
		stream >> inDay >> separator >> inMonth >> separator >> inYear;

		if (inYear >= year &&
			inMonth >= month &&
			31 >=inMonth &&
			1 <= inMonth &&
			inDay >= day)
		{
			date = Date(inDay, inMonth, inYear);
			validDate = true;
		}
	}
	return validDate;
};

bool Validation::isPhoneFormatValid(std::string& phoneNumber)
{
	return std::regex_match(phoneNumber, mPhoneNumberFormat);
}

bool Validation::isPhoneFormatValid(CellNumber& phone, std::string& phoneNumber)
{

	bool status = std::regex_match(phoneNumber, mPhoneNumberFormat);

	if (status)
	{
		std::stringstream stream(phoneNumber);
		std::string areaCode, xChange, line;
		char separator;
		stream >> areaCode >> separator >> xChange >> separator >> line;
		phone = CellNumber(areaCode, xChange, line);
	}
	return status;
}

bool Validation::isValidDate(std::string& date)
{
	bool result = isValidDateFormat(date);


	return false;
}

bool Validation::isValidDateFormat(std::string& inDate)
{
	return std::regex_match(inDate, mDateFormat);
}
