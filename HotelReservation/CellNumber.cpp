#include "CellNumber.h"
#include <iostream>
CellNumber::CellNumber()
	:mAreaCode(""),
	mXChange(""),
	mLine("")
{
}
CellNumber::CellNumber(const CellNumber &cell)
	:mAreaCode(cell.mAreaCode),
	mXChange(cell.mXChange),
	mLine(cell.mLine)
{}

CellNumber::CellNumber(std::string& areaCode, std::string& xChange, std::string& line) :
	mAreaCode(areaCode),
	mXChange(xChange),
	mLine(line)
{}

//setters

void CellNumber::setAreaCode(const std::string& areaCode){mAreaCode=areaCode;}
void CellNumber::setXchange(const std::string& xChnge){mXChange=xChnge;}
void CellNumber::setLine(const std::string& line){mLine=line;}
//getters
std::string CellNumber::getAreaCode() const {return mAreaCode;}
std::string CellNumber::getXchange()const {return mXChange;}
std::string CellNumber::getLine()const {return mLine;}

/*
//sectionalizes number area code, etc
CellNumber CellNumber::separate(char *number){

	char *temp="";
	const int max=50;
	char  areaCode[max];
	char xchange[max];
	char line[max];
	int b=0, a=0,c=0;//i;terators
	
	temp=remover(number);

		for(int i=0; i <10; i++)
		{
			if(i<3){
				areaCode[a]=temp[i];
				a++;
			}
			if(i == 3)
				areaCode[a]='\0';
			
			if(i>2 && i<6){				
				xchange[b]=temp[i];
				b++;				
			}
			if(i == 6)
				xchange[b]='\0';

			if(i>5 && i<10){
				line[c]=temp[i];
				c++;				
			}
			if(i == 9)
				line[c]='\0';			
		}

		CellNumber cell = CellNumber();
		cell.setAreaCode(areaCode);
		cell.setXchange(xchange);
		cell.setLine(line);	
	return cell;		
}
*/

//removes spaces and dashes
char * CellNumber::remover(char * num){
	int cnt=0;
		
	for(int i=0; i < (signed)strlen(num); i++)
	{
		if((num[i] != ' ') && (num[i] != '-'))
			num[cnt++] = num[i];
	}
	num[cnt]='\0';

	return num;
}
/**/

void CellNumber::displayNum(){

	std::cout<< getAreaCode()<< getXchange() << getLine()<<std::endl;
}

std::string CellNumber::getNumString() const
{
	return mAreaCode + mXChange + mLine;
}


std::ostream& operator<<(std::ostream& write, const CellNumber& phoneNum)
{
	write << phoneNum.mAreaCode << "\n"
		<< phoneNum.mXChange << "\n"
		<< phoneNum.mLine;

	return write;
}

std::istream& operator>>(std::istream& read, CellNumber& phoneNum)
{
	read >> phoneNum.mAreaCode
		>> phoneNum.mXChange
		>> phoneNum.mLine;

	return read;
}

