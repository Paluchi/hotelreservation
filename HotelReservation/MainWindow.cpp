#include "MainWindow.h"
#include "Validation.h"

MainWindow::MainWindow()
	:mResManager(std::make_unique<ReservationManager>()),
	mAccount(std::make_unique<Account>())
{

}

void MainWindow::heading()
{
	clrscrn;
	std::cout << "\n\t\t\t\t" << h1 << std::endl;
	std::cout << "\n\t\t\t" << h2 << std::endl;
	mAccount->displayUser();


}

char MainWindow::headingOption()
{	
	char option = ' ';
	std::cout << "Select Reservation Type" << std::endl;
	std::cout << "1: Hotel\n2: Spa\nAny key to Exit" << std::endl;
	std::cin >> option;
	return option;
}

char MainWindow::hotelOptions()
{
	char option = ' ';
	std::cout << "\n\nSelect option" << std::endl;
	std::cout << "\n1: Make Reservation" << std::endl;
	std::cout << "\n2: View All Reservation" << std::endl;
	std::cout << "\n3: Reservation Lookup" << std::endl;
	std::cout << "\n4: Update A Reservation\n";
	std::cin >> option;

	return option;
}

char MainWindow::makeRoomReservation()
{
	char option = ' ';
	clrscrn;
	heading();
	if (mResManager->makeRoomReservation())
		std::cout << "Added" << std::endl;
	else
		std::cerr << "Not Added" << std::endl;

	std::cout << "\n1: Continue\nAny key Exit Reservations\n" << std::endl;
	std::cin >> option;
	return option;
}

char MainWindow::getAllHotelRoomReservations()
{
	char option = ' ';
	clrscrn;
	heading();
	mResManager->displayRoomReservations();
	std::cout << "\n1: Continue\nAny key Exit Reservations\n" << std::endl;
	std::cin >> option;
	clrscrn;
	return option;
}

char MainWindow::getReservation(const std::string& key)
{	
	char option{ ' ' };
	clrscrn;
	heading();	
	(mResManager->getRoomReservation(key)).display();
	std::cout << "\n1: Continue\nAny key Exit Reservations\n" << std::endl;
	std::cin >> option;
	return option;
}

//TODO CHECK UPDATING 
char MainWindow::updateAReservation()
{	
	std::string key{};
	char option{ ' ' };
	heading();
	std::cout << "Phone Number: XXX-XXX-XXXX\n";
	std::cin >> key;
	Validation validator;
	if (validator.isPhoneFormatValid(key))
	{
		if (mResManager->updateRoomReservation(key))
			std::cout << "Record Updated\n";
	}
	else
		std::cerr << "Unable to update Record\n";

	std::cout << "\n1: Continue\nAny key Exit Reservations\n";
	std::cin >> option;
	return option;
}

//Spa treatment options Walkin /guests
char MainWindow::spaOption()
{
	char option = ' ';
	std::cout << "1:Hotel Guests\n2:Walkin Guest\n";
	std::cin >> option;
	return option;
}

char MainWindow::makeGuest()
{
	char option{ ' ' };
	heading();
	mResManager->makeGuestSpaAppoinment();

	std::cout << "\n1: Continue\nAny key Exit Spa Reservations\n";
	std::cin >> option;
	return option;

}

char MainWindow::makeWalk_In()
{
	char option{ ' ' };
	heading();
	mResManager->makeWalkinAppointment();
	std::cout << "\n1: Continue\nAny key Exit Spa Reservations\n" << std::endl;
	std::cin >> option;
	return option;

}

void MainWindow::finalWindow()
{
	char option = '1';

	while (option == '1' || option == '2')
	{
		char hotel = '1', spa = '1', key[] = "          ";

		heading();
		option = headingOption();

		switch (option)
		{
		case '1':

			while (hotel == '1')
			{

				heading();
				hotel = hotelOptions();
				switch (hotel)
				{
				case '1':
					hotel = makeRoomReservation();
					break;
				case '2':
					hotel = getAllHotelRoomReservations();
					break;
				case '3':
					std::cout << "Enter Phone Number" << std::endl;
					std::cin >> key;
					hotel = getReservation(key);
					break;
				case '4':
					hotel = updateAReservation();
					break;
				default:
					std::cerr << invalid << std::endl;
					std::cout << "\n1: Continue\nAny key Exit Reservations\n" << std::endl;
					std::cin >> hotel;
					break;
				}


			}


			break;
		case '2':
			while (spa == '1')
			{
				spa = spaOption();
				switch (spa)
				{
				case '1':
					spa = makeGuest();
					break;
				case '2':
					spa = makeWalk_In();
					break;
				default:
					std::cerr << invalid << std::endl;
					std::cout << "\n1: Continue\nAny key Exit Spa Reservations\n" << std::endl;
					std::cin >> spa;
				}
			}

			break;
		default:
			heading();
			std::cout << "\n\t\t\t\tGOODBYE!!!!" << std::endl;
			break;

		}//ends main switch

	}//end program while
}

bool MainWindow::login()
{
	std::string input{};

	std::cout << "username\n";
	std::getline(std::cin, input);
	mAccount->setUsername(input);
	std::cout << "\nPassword\n";
	std::getline(std::cin, input);
	mAccount->setPassword(input);

	return  mAccount->validUser("paluchi", "password");;
}