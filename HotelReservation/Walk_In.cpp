#include "Walk_In.h"

std::ofstream& operator<<(std::ofstream & write, const Walk_In & walkIn)
{
	write << walkIn.mFName << std::endl
		<< walkIn.mLName << std::endl
		<< walkIn.mResTime << std::endl
		<< walkIn.mResDate << std::endl
		<< walkIn.mPhoneNumber << std::endl
		<< walkIn.mService << std::endl;

	return write;
}

std::ifstream& operator>>(std::ifstream & read, Walk_In & walkIn)
{
	read >> walkIn.mFName
		>> walkIn.mLName
		>> walkIn.mResTime
		>> walkIn.mResDate
		>> walkIn.mPhoneNumber
		>> walkIn.mService;
	return read;
}

void Walk_In::makeSpaReservation()
{
	Spa::makeSpaReservation();
	char choice = 49;

	std::string input{};
	do {		
		std::cout << "\nPhone Number: Please use the format XXX-XXX-XXXX\n";
		std::getline(std::cin, input);
		if (mValidator.isPhoneFormatValid(input))
		{
			mPhoneNumber.setAreaCode(std::string(input.begin(), input.begin() + 3));
			mPhoneNumber.setXchange(std::string(input.begin() + 5, input.begin() + 7));
			mPhoneNumber.setLine(std::string(input.begin() + 9, input.end()));

			break;
		}
		std::cout << "\nInvalid Format detected\nPress 1 to Continue\nor\n Press 0 to exit\n";
		std::getline(std::cin, input);
		
	} while (choice != 48);
}
