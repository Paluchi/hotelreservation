#ifndef DATE_H
#define DATE_H
#include <fstream>
class Date{

private:
	int mMonth;
	int mDay;
	int mYear;

public: 
		Date(int day, int month, int year);
		Date();
		Date(const Date&);
		void displayDate();
		//getters
		int getDay() const ;
		int getMonth() const ;
		int getYear() const ;
		//setters
		void setDay(const int);
		void setMonth(const int);
		void setYear(const int);

		std::string getStringDate();
		
		friend std::ostream& operator<<(std::ostream& write, const Date& date);
		friend std::istream& operator>>(std::istream& read, Date& date);
		
		

		

};
#endif 
