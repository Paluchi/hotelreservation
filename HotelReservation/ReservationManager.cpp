#include "ReservationManager.h"
#

ReservationManager::ReservationManager()
	:mGuest(std::make_unique<Guest>()),
	mWalkIn(std::make_unique<Walk_In>())
{
	mFileTask = std::async(std::launch::async, &ReservationManager::populateRoomReservations, this);
	mWalkInReadTask = std::async(std::launch::async, &ReservationManager::populateWalkInAppointments, this);
	mGuestReadTask = std::async(std::launch::async, &ReservationManager::populateGuestAppointments, this);
}

ReservationManager::~ReservationManager()
{		
	mStopFileTasks.store(true, std::memory_order_release);	

	auto roomF = std::async(std::launch::async, &ReservationManager::saveRoomReservations, this);
	auto walkinF = std::async(std::launch::async, &ReservationManager::saveWalkInReservations, this);
	auto guestF = std::async(std::launch::async, &ReservationManager::saveGuestSpaReservations, this);	
	
	roomF.get();
	walkinF.get();
	guestF.get();	
}

void ReservationManager::populateRoomReservations()
{
	std::unique_lock<std::mutex> reservationLock(resFileMutex);	
	try {
		std::ifstream read(resFile, std::ios::in);
		if (read)
		{
			while (!read.eof())
			{
				if (mStopFileTasks.load(std::memory_order_acquire))
				{
					break;
				}
				RoomReservation reservation;
				read >> reservation;
				mRoomReservations.emplace(reservation.getPhoneNum()->getNumString(), reservation);
			}
			if (read.eof())
			{
				mReservationP.set_value(1);
			}
			read.close();
		}
		else
		{
			throw std::ios_base::failure("Unable to open reservation file");
		}
	}
	catch (...)
	{
		mReservationP.set_value(0);
		mReservationP.set_exception(std::current_exception());	
	}
}


void ReservationManager::displayRoomReservations()
{
	try {
		if (mReservationF.get() == 1)
		{
			for (auto& item : mRoomReservations)
			{
				item.second.display();
			}
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;

	}
	
	
}

bool ReservationManager::makeRoomReservation()
{
	std::unique_lock<std::mutex> resLock(mReservationsMtx);
	RoomReservation room;
	room = room.getInfo();
	
	auto result = mRoomReservations.emplace(room.getPhoneNum()->getNumString(), room);
	return result.second;
}

RoomReservation ReservationManager::getRoomReservation(const std::string& phoneNumber)
{
	return mRoomReservations.at(phoneNumber);
}


bool ReservationManager::updateRoomReservation(const std::string& phoneNumber)
{
	bool result{ false };
	try {
		auto& res = mRoomReservations.at(phoneNumber);
		res.display();
		//TODO syntax for Editing then save
		result = true;
	}
	catch (std::out_of_range& e)
	{
		std::cerr << "Update failed\t" << e.what() << std::endl;
	}
	return result;
}


void ReservationManager::saveRoomReservations()
{
	if (mFileTask.valid())
	{
		mFileTask.wait();
	}
	std::unique_lock<std::mutex> saveLock(resFileMutex);
	
	std::ofstream write(resSaveFile, std::ios::out);
	if (write)
	{
		for (const auto& item : mRoomReservations)
		{
			write << item.second;
		}
		write.close();
	}
}


//handles Emplace error
void ReservationManager::populateGuestAppointments()
{
	std::unique_lock<std::mutex> guestFileLck(mGuestFileMutex);
	std::ifstream read(guestFile, std::ios::in);

	if (read)
	{
		while (!read.eof())
		{
			if (mStopFileTasks.load(std::memory_order_acquire))
			{
				break;
			}
			Guest guest;

			read >> guest;
			mGuestSpaReservations.emplace(guest.getRoomNum(), guest);
		}
		if (read.eof())
		{
			mGuestResP.set_value(1);
		}
		read.close();
	}
}

bool ReservationManager::makeGuestSpaAppoinment()
{
	mGuest->makeSpaReservation();	
	auto result = mGuestSpaReservations.emplace(mGuest->getPhoneNumber().getNumString(), *mGuest);
	return result.second;
}

void ReservationManager::saveGuestSpaReservations()
{
	if (mGuestReadTask.valid())
	{
		mGuestReadTask.wait();
	}
	if (mGuestResP.get_future().get())
	{
		std::unique_lock<std::mutex> guestFileLck(mGuestFileMutex);
		std::ofstream write(guestFile, std::ios::out);
		if (write)
		{
			for (const auto& item : mGuestSpaReservations)
			{
				write << item.second;
			}
			write.close();
		}
	}
}

void ReservationManager::populateWalkInAppointments()
{
	std::unique_lock<std::mutex> walkinFileLck(mWalkinFileMutex);
	std::ifstream read(walkinFile, std::ios::in);
	if (read)
	{
		while (!read.eof())
		{
			if (mStopFileTasks.load(std::memory_order_acquire))
			{
				break;
			}
			Walk_In walkIn;
			mWalkInSpaReservations.emplace(walkIn.getPhoneNumber().getNumString(), walkIn);
		}
		if (read.eof())
		{
			mWalkinResP.set_value(1);
		}
		read.close();
	}
}

bool ReservationManager::makeWalkinAppointment()
{
	
	mWalkIn->makeSpaReservation();
	auto status = mWalkInSpaReservations.emplace(mWalkIn->getPhoneNumber().getNumString(), *mWalkIn);
	

	return status.second;
}

void ReservationManager::saveWalkInReservations()
{
	if (mWalkInReadTask.valid())
	{
		mWalkInReadTask.wait();
	}
	if (mWalkinResP.get_future().get())
	{
		std::unique_lock<std::mutex> walkinFileLck(mWalkinFileMutex);

		std::ofstream write(walkinFile, std::ios::out);
		if (write)
		{
			for (auto& itemP : mWalkInSpaReservations)
			{
				write << itemP.second;
			}
			write.close();
		}
	}
}