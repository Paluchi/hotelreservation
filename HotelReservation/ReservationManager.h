#pragma once
#ifndef RESERVATIONMANAGER_H
#define RESERVATIONMANAGER_H
#include "RoomReservation.h"
#include "Guest.h"
#include "Walk_In.h"
#include <unordered_map>
#include <algorithm>
#include <future>
#include <mutex>

class ReservationManager final
{
public:
	ReservationManager();
	~ReservationManager();

	//Room Reservation
	bool makeRoomReservation();
	RoomReservation getRoomReservation(const std::string & phoneNumber);
	void displayRoomReservations();
	bool updateRoomReservation(const std::string& phoneNumber);
	
	//Guest
	bool makeGuestSpaAppoinment();

	//Walin
	bool makeWalkinAppointment();


private:
	static constexpr const char* resFile{ "reservation.txt" };
	static constexpr const char* resSaveFile{ "reservationT.txt" };
	static constexpr const char* guestFile{ "Guest.txt" };
	static constexpr const char* walkinFile{ "Walkin.txt" };
	std::unordered_map<std::string, RoomReservation> mRoomReservations;
	std::unordered_map<std::string, Guest> mGuestSpaReservations;
	std::unordered_map<std::string, Walk_In> mWalkInSpaReservations;
	void populateRoomReservations();
	void saveRoomReservations();
	void saveGuestSpaReservations();
	void saveWalkInReservations();
	void populateGuestAppointments();
	void populateWalkInAppointments();
	std::mutex resFileMutex;
	std::mutex mGuestFileMutex;
	std::mutex mWalkinFileMutex;
	std::mutex mReservationsMtx;
	//Promises
	std::promise<int> mReservationP;
	std::promise<int> mWalkinResP;
	std::promise<int> mGuestResP;
	//futures
	std::future<int> mReservationF = mReservationP.get_future();
	//futures for the Tasks
	std::future<void> mFileTask;
	std::future<void> mGuestReadTask;
	std::future<void> mWalkInReadTask;
	//Interrupts 
	std::atomic<bool> mStopFileTasks{ false };
	std::unique_ptr<Guest> mGuest;
	std::unique_ptr<Walk_In> mWalkIn;

};
#endif

