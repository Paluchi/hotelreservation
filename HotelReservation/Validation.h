#ifndef VALIDATION_H
#define VALIDATION_H

#include "Date.h"
#include "CellNumber.h"
#include "Validation.h"
#include <regex>
class Validation{

public:
	Validation() = default;

	bool checkDate(Date& date, std::string& inDate);
	
	bool isPhoneFormatValid(std::string& phoneNumber);

	bool Validation::isPhoneFormatValid(CellNumber& phone, std::string& phoneNumber);

	bool isValidDate(std::string& date);

private:
	 const std::regex mDateFormat{ "^\\d{2}-\\d{2}-\\d{4}$" };
	 const std::regex mPhoneNumberFormat{ "^\\d{3}-\\d{3}-\\d{4}$" };

	 bool isValidDateFormat(std::string& inDate);

};
#endif