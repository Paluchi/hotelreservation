#ifndef CELLNUMBER_H
#define CELLNUMBER_H
#include <string>
#include <fstream>
class CellNumber{
private:
	std::string mAreaCode;
	std::string mXChange;
	std::string mLine;

public:
	CellNumber(std::string& areaCode, std::string& xChange, std::string& line);
	CellNumber();
	CellNumber(const CellNumber& );	

	void setAreaCode(const std::string& areaCode);
	void setXchange(const std::string& xChange);
	void setLine(const std::string& line);
	std::string getAreaCode()const ;
	std::string getXchange() const ;
	std::string getLine() const ;
	//CellNumber separate(char *);
	char *remover(char*);
	void displayNum();
	std::string getNumString() const;


	friend std::ostream& operator<<(std::ostream& write, const CellNumber& phoneNum);
	friend std::istream& operator>>(std::istream& read, CellNumber& phoneNum);
};
#endif;